package Presentation_classes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import Business_Logic_classes.ClientBLL;
import Business_Logic_classes.ProductBLL;
import Data_access_classes.ClientDAO;
import Data_access_classes.ProductDAO;
import Model_classes.Client;
import Model_classes.Product;

public class ProductProcessing implements ActionListener{

	private JFrame frame = new JFrame("Product Processing");
	
	private JPanel info = new JPanel();
	private JPanel actions = new JPanel();
	private JPanel table = new JPanel();

	private JLabel idLabel = new JLabel("Product ID:");
	private JLabel nameLabel = new JLabel("Name:");
	private JLabel priceLabel = new JLabel("Price:");
	private JLabel stockLabel = new JLabel("Stock:");

	private JTextField idText = new JTextField();
	private JTextField nameText = new JTextField();
	private JTextField priceText = new JTextField();
	private JTextField stockText = new JTextField();

	private int P_ID;
	private String P_Name;
	private double price;
	private int stock;

	private JButton addProduct = new JButton("Add Product");
	private JButton updatePrice = new JButton("Update Price");
	private JButton deleteProduct = new JButton("Delete Product");
	private JButton view = new JButton("View Table");
	
	private JTable productsTable;
	ArrayList<Product> products = ProductDAO.findAllProducts();
	
	private JScrollPane scrollPane;

	public  ProductProcessing() {

		frame.setSize(700, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		frame.setVisible(true);

		info.setLayout(new GridLayout(4, 2));
		info.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		info.setVisible(true);

		info.add(idLabel);
		info.add(idText);
		info.add(nameLabel);
		info.add(nameText);
		info.add(priceLabel);
		info.add(priceText);
		info.add(stockLabel);
		info.add(stockText);

		idText.addActionListener(this);
		nameText.addActionListener(this);
		priceText.addActionListener(this);
		stockText.addActionListener(this);
		
		actions.setLayout(new FlowLayout());
		
		actions.add(addProduct);
		actions.add(updatePrice);
		actions.add(deleteProduct);
		actions.add(view);
		
		addProduct.setActionCommand("add");
		updatePrice.setActionCommand("update");
		deleteProduct.setActionCommand("delete");
		view.setActionCommand("view");
		
		addProduct.setBackground(Color.lightGray);
		updatePrice.setBackground(Color.lightGray);
		deleteProduct.setBackground(Color.lightGray);
		view.setBackground(Color.lightGray);
		
		addProduct.addActionListener(this);
		updatePrice.addActionListener(this);
		deleteProduct.addActionListener(this);
		view.addActionListener(this);
		
		table.setLayout(new FlowLayout());

		frame.add(info, BorderLayout.NORTH);
		frame.add(actions, BorderLayout.CENTER);
		
	}
	
	public  void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("add")) {
			P_ID = Integer.parseInt(idText.getText());
			P_Name = nameText.getText();
			price = Double.parseDouble(priceText.getText());
			stock = Integer.parseInt(stockText.getText());
			
			Product p = new Product(P_ID, price, P_Name, stock);
			int id = ProductBLL.addProduct(p);
		}
		else if (e.getActionCommand().equals("update")) {
			P_ID = Integer.parseInt(idText.getText());
			P_Name = nameText.getText();
			price = Double.parseDouble(priceText.getText());
			stock = Integer.parseInt(stockText.getText());
			
			ProductBLL.updatePrice(P_ID, price);
			
		}
		else if (e.getActionCommand().equals("delete")) {
			P_ID = Integer.parseInt(idText.getText());
			P_Name = nameText.getText();
			price = Double.parseDouble(priceText.getText());
			stock = Integer.parseInt(stockText.getText());
			
			ProductBLL.deleteProduct(P_ID);
			
		}
		else if (e.getActionCommand().equals("view")) {
			
			productsTable = ProductDAO.buildTable(products);
			
			scrollPane = new JScrollPane(productsTable);
			
			scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			//scrollPane.setAutoscrolls(true);
			scrollPane.setVisible(true);
			
			table.add(scrollPane);
			
			frame.add(table, BorderLayout.SOUTH);
			
		}
	}


}
