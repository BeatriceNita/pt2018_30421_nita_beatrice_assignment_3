package Presentation_classes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Model_classes.Client;
import Model_classes.Order;
import Model_classes.Product;
import connection.ConnectionFactory;

public class NrRows {

	private static String statement1 = "SELECT COUNT(C_ID) AS count FROM client";
	private static String statement2 = "SELECT COUNT(P_ID) AS count1 FROM product";
	private static String statement3 = "SELECT COUNT(O_ID) AS count2 FROM ordermanagement.order";
	
	public static int countRowsClient(Client client) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement countC = null;
		ResultSet rs = null;
		int count = 0;
		
			try {
				countC = dbConn.prepareStatement(statement1);
				
				rs = countC.executeQuery();
				rs.next();
				
				count = rs.getInt("count");
				    
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return count;
	}
	
	public static int countRowsProduct(Product product) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement countP = null;
		ResultSet rs = null;
		int count = 0;
		
			try {
				countP = dbConn.prepareStatement(statement2);
				
				rs = countP.executeQuery();
				rs.next();
				
				count = rs.getInt("count1");
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		return count;
	}
	
	public static int countRowsOrder(Order order) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement countO = null;
		ResultSet rs = null;
		int count = 0;
		
			try {
				countO = dbConn.prepareStatement(statement3);
				
				rs = countO.executeQuery();
				rs.next();
				
				count = rs.getInt("count2");

			} catch (SQLException e) {
				e.printStackTrace();
			}
		return count;
	}
}
