package Presentation_classes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import Business_Logic_classes.OrderBLL;
import Business_Logic_classes.ProductBLL;
import Data_access_classes.ClientDAO;
import Data_access_classes.OrderDAO;
import Data_access_classes.ProductDAO;
import Model_classes.Order;
import Model_classes.Product;

public class OrderProcessing implements ActionListener{

	private JFrame frame = new JFrame("Order Processing");
	
	private JPanel info = new JPanel();
	private JPanel actions = new JPanel();
	private JPanel table = new JPanel();

	private JLabel idLabel = new JLabel("Order ID:");
	private JLabel idLabel1 = new JLabel("Client ID:");
	private JLabel idLabel2 = new JLabel("Product ID:");
	private JLabel quantityLabel = new JLabel("Quantity:");

	private JTextField idText = new JTextField();
	private JTextField idText1 = new JTextField();
	private JTextField idText2 = new JTextField();
	private JTextField quantityText = new JTextField();

	private int O_ID;
	private int C_ID;
	private int P_ID;
	private int quantity;

	private JButton addOrder = new JButton("Add Order");
	private JButton updateOrder = new JButton("Update Order");
	private JButton deleteOrder = new JButton("Delete Order");
	private JButton view = new JButton("View Table");
	
	private JTable ordersTable;
	ArrayList<Order> orders = OrderDAO.findAllOrders();
	
	private JScrollPane scrollPane;

	public  OrderProcessing() {

		frame.setSize(700, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		frame.setVisible(true);

		info.setLayout(new GridLayout(4, 2));
		info.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		info.setVisible(true);

		info.add(idLabel);
		info.add(idText);
		info.add(idLabel1);
		info.add(idText1);
		info.add(idLabel2);
		info.add(idText2);
		info.add(quantityLabel);
		info.add(quantityText);

		idText.addActionListener(this);
		idText1.addActionListener(this);
		idText2.addActionListener(this);
		quantityText.addActionListener(this);
		
		actions.setLayout(new FlowLayout());
		
		actions.add(addOrder);
		actions.add(updateOrder);
		actions.add(deleteOrder);
		actions.add(view);
		
		addOrder.setActionCommand("add");
		updateOrder.setActionCommand("update");
		deleteOrder.setActionCommand("delete");
		view.setActionCommand("view");
		
		addOrder.setBackground(Color.YELLOW);
		updateOrder.setBackground(Color.YELLOW);
		deleteOrder.setBackground(Color.YELLOW);
		view.setBackground(Color.YELLOW);
		
		addOrder.addActionListener(this);
		updateOrder.addActionListener(this);
		deleteOrder.addActionListener(this);
		view.addActionListener(this);
		
		table.setLayout(new FlowLayout());
		
		frame.add(info, BorderLayout.NORTH);
		frame.add(actions, BorderLayout.CENTER);
		
	}
	
	public  void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("add")) {
			O_ID = Integer.parseInt(idText.getText());
			C_ID = Integer.parseInt(idText1.getText());
			P_ID = Integer.parseInt(idText2.getText());
			quantity = Integer.parseInt(quantityText.getText());
			
			Order o = new Order(O_ID, C_ID, P_ID, quantity);
			int id = OrderBLL.addOrder(o);
		}
		else if (e.getActionCommand().equals("update")) {
			O_ID = Integer.parseInt(idText.getText());
			C_ID = Integer.parseInt(idText1.getText());
			P_ID = Integer.parseInt(idText2.getText());
			quantity = Integer.parseInt(quantityText.getText());
			
			OrderBLL.updateOrder(O_ID,C_ID,P_ID,quantity);
			
		}
		else if (e.getActionCommand().equals("delete")) {
			O_ID = Integer.parseInt(idText.getText());
			C_ID = Integer.parseInt(idText1.getText());
			P_ID = Integer.parseInt(idText2.getText());
			quantity = Integer.parseInt(quantityText.getText());
			
			OrderBLL.deleteOrder(O_ID);
			
		}
		else if (e.getActionCommand().equals("view")) {
			
			ordersTable = OrderDAO.buildTable(orders);
			
			scrollPane = new JScrollPane(ordersTable);
			
			scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			//scrollPane.setAutoscrolls(true);
			scrollPane.setVisible(true);
			
			table.add(scrollPane);
			
			frame.add(table, BorderLayout.SOUTH);
		}
	}



}
