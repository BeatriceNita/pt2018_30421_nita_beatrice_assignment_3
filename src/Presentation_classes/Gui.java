package Presentation_classes;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Gui implements ActionListener{

	private JFrame frame = new JFrame();
	private JPanel panel = new JPanel();
	
	private JButton ClientProc;
	private JButton ProductProc;
	private JButton OrderProc;
	
	public Gui() {
		frame.setTitle("Order Management");//the title of the frame
		frame.setVisible(true);//very important! we want to see all the components of our frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//this is meant for when the close button is pushed, the frame to close
		frame.setSize(740, 260);//the size of the frame
		frame.setLocationRelativeTo(null);//it places the cursor at the center of the frame
		
		ClientProc = new JButton("Client Processing");
		ProductProc = new JButton("Product Processing");
		OrderProc = new JButton("Order Processing");
		
		ClientProc.setActionCommand("process client");
		ProductProc.setActionCommand("process product");
		OrderProc.setActionCommand("process order");
		
		ClientProc.addActionListener(this);
		ProductProc.addActionListener(this);
		OrderProc.addActionListener(this);
		
		ClientProc.setBackground(Color.CYAN);
		ClientProc.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		ProductProc.setBackground(Color.lightGray);
		ProductProc.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		OrderProc.setBackground(Color.YELLOW);
		OrderProc.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		panel.setLayout(new GridLayout(1,3));
		panel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		panel.setVisible(true);
		
		panel.add(ClientProc);
		panel.add(ProductProc);
		panel.add(OrderProc);
		
		frame.add(panel);
	}
	
	public  void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("process client")) {
			ClientProcessing c = new ClientProcessing();
		}
		else if (e.getActionCommand().equals("process product")) {
			ProductProcessing p = new ProductProcessing();
		}
		else if (e.getActionCommand().equals("process order")) {
			OrderProcessing o = new OrderProcessing();
		}
	}
	
}
