package Presentation_classes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import Business_Logic_classes.ClientBLL;
import Data_access_classes.ClientDAO;
import Model_classes.Client;

public class ClientProcessing implements ActionListener{

	private JFrame frame = new JFrame("Client Processing");
	
	private JPanel info = new JPanel();
	private JPanel actions = new JPanel();
	private JPanel table = new JPanel();

	private JLabel idLabel = new JLabel("Client ID:");
	private JLabel nameLabel = new JLabel("Name:");
	private JLabel ageLabel = new JLabel("Age:");
	private JLabel addressLabel = new JLabel("Address:");

	private JTextField idText = new JTextField();
	private JTextField nameText = new JTextField();
	private JTextField ageText = new JTextField();
	private JTextField addressText = new JTextField();

	private int C_ID;
	private String C_name;
	private int age;
	private String address;

	private JButton addClient = new JButton("Add Client");
	private JButton updateClient = new JButton("Update Client");
	private JButton deleteClient = new JButton("Delete Client");
	private JButton view = new JButton("View Table");
	
	private JTable clientsTable;
	ArrayList<Client> clients = ClientDAO.findAllClients();
	
    private JScrollPane scrollPane;

	public  ClientProcessing() {

		frame.setSize(700, 700);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setLayout(new BorderLayout());
		frame.setVisible(true);

		info.setLayout(new GridLayout(5, 2));
		info.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		info.setVisible(true);

		info.add(idLabel);
		info.add(idText);
		info.add(nameLabel);
		info.add(nameText);
		info.add(ageLabel);
		info.add(ageText);
		info.add(addressLabel);
		info.add(addressText);

		idText.addActionListener(this);
		nameText.addActionListener(this);
		ageText.addActionListener(this);
		addressText.addActionListener(this);
		
		actions.setLayout(new FlowLayout());
		
		actions.add(addClient);
		actions.add(updateClient);
		actions.add(deleteClient);
		actions.add(view);
		
		addClient.setActionCommand("add");
		updateClient.setActionCommand("update");
		deleteClient.setActionCommand("delete");
		view.setActionCommand("view");
		
		addClient.setBackground(Color.CYAN);
		updateClient.setBackground(Color.CYAN);
		deleteClient.setBackground(Color.CYAN);
		view.setBackground(Color.CYAN);
		
		addClient.addActionListener(this);
		updateClient.addActionListener(this);
		deleteClient.addActionListener(this);
		view.addActionListener(this);
		
		table.setLayout(new FlowLayout());
		
		frame.add(info, BorderLayout.NORTH);
		frame.add(actions, BorderLayout.CENTER);
		
	}
	
	public  void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("add")) {
			C_ID = Integer.parseInt(idText.getText());
			C_name = nameText.getText();
			age = Integer.parseInt(ageText.getText());
			address = addressText.getText();
			
			Client c = new Client(C_ID, C_name, age, address);
			int id = ClientBLL.addClient(c);
		}
		else if (e.getActionCommand().equals("update")) {
			C_ID = Integer.parseInt(idText.getText());
			C_name = nameText.getText();
			age = Integer.parseInt(ageText.getText());
			address = addressText.getText();
			
			ClientBLL.updateClient(C_ID, C_name, age, address);
		}
		else if (e.getActionCommand().equals("delete")) {
			C_ID = Integer.parseInt(idText.getText());
			C_name = nameText.getText();
			age = Integer.parseInt(ageText.getText());
			address = addressText.getText();
			
			ClientBLL.deleteClient(C_ID);
		}
		else if (e.getActionCommand().equals("view")) {
			
			clientsTable = ClientDAO.buildTable(clients);
			
			scrollPane = new JScrollPane(clientsTable);
			
			scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
			//scrollPane.setViewportView(clientsTable);
			//scrollPane.setAutoscrolls(true);
			scrollPane.setVisible(true);
			
			table.add(scrollPane);
			
			frame.add(table, BorderLayout.SOUTH);
		}
	}

	
}
