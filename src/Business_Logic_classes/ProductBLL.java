package Business_Logic_classes;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import Data_access_classes.ProductDAO;
import Model_classes.Product;

public class ProductBLL {

	public static int addProduct(Product product) {
		return ProductDAO.insertProduct(product);
	}
	
	public static void updatePrice(int P_ID, double price) {
		ProductDAO.updatePrice(P_ID, price);
	}
	
	public static void updateStock(int P_ID, int stock) {
		Product p = findProductByID(P_ID);
		int newStock = p.getStock() - stock;
		ProductDAO.updateStock(P_ID, newStock);
	}
	
	public static void deleteProduct(int P_ID) {
		ProductDAO.deleteProduct(P_ID);
	}
	
	public static Product findProductByID(int P_ID) {
		Product product = ProductDAO.findByID(P_ID);
		if (product == null) {
				throw new NoSuchElementException("The product with id =" + P_ID + " was not found!");
		}
		return product;
	}
	
	public static ArrayList<Product> getAllProducts(){
		ArrayList<Product> findAll = new ArrayList<Product>();
		findAll = ProductDAO.findAllProducts();
		return findAll;
    }
}
