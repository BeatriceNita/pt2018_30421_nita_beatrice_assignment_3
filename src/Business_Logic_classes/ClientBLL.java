package Business_Logic_classes;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.swing.JTable;

import Data_access_classes.ClientDAO;
import Model_classes.Client;

public class ClientBLL {
	
	private static ArrayList<Client> clients = new ArrayList<Client>();

	public static int addClient(Client client) {
		return ClientDAO.insertClient(client);
	}
	
	public static void updateClient(int C_ID, String C_Name, int age, String address) {
		ClientDAO.updateClient(C_ID, C_Name, age, address);
	}
	
	public static void deleteClient(int C_ID) {
		ClientDAO.deleteClient(C_ID);
	}
	
	public static Client findClientByID(int C_ID) {
		Client client = ClientDAO.findByID(C_ID);
		if (client == null) {
				throw new NoSuchElementException("The client with id =" + C_ID + " was not found!");
		}
		return client;
	}
	
	public static ArrayList<Client> getAllClients(){
		ArrayList<Client> findAll = new ArrayList<Client>();
		findAll = ClientDAO.findAllClients();
		return findAll;
    }
	
	public static JTable createTable() {
		return ClientDAO.buildTable(clients);
	}
}
