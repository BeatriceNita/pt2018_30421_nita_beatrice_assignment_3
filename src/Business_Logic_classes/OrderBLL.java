package Business_Logic_classes;

import Data_access_classes.ClientDAO;
import Data_access_classes.OrderDAO;
import Data_access_classes.ProductDAO;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import javax.swing.JOptionPane;

import Business_Logic_classes.ProductBLL;
import Model_classes.Client;
import Model_classes.Order;
import Model_classes.Product;

public class OrderBLL {

	public static int addOrder(Order order) {
		
		Product p = ProductDAO.findByID(order.getP_ID());
		Client c = ClientDAO.findByID(order.getC_ID());
		
		if ( (p == null) || (c == null)) 
		{
			JOptionPane.showMessageDialog(null, "THE CLIENT OR PRODUCT CANNOT BE FOUND!");
			return 0;
		}
		else if(order.getQuantity() >= p.getStock()) 
		{
			JOptionPane.showMessageDialog(null, "NOT ENOUGH PRODUCTS IN STOCK!");
			return 0;
		}
		else return OrderDAO.insertOrder(order);
	}
	
	public static void updateOrder(int O_ID, int C_ID, int P_ID, int quantity) {
		OrderDAO.updateOrder(O_ID, C_ID, P_ID, quantity);
	}
	
	public static void deleteOrder(int O_ID) {
		OrderDAO.deleteOrder(O_ID);
	}
	
	/*public static Order findOrderByID(int O_ID) {
		Order order = OrderDAO.findByID(O_ID);
		if (order == null) {
				throw new NoSuchElementException("The order with id =" + O_ID + " was not found!");
		}
		return order;
	}*/
	
	public static ArrayList<Order> getAllOrders(){
		ArrayList<Order> findAll = new ArrayList<Order>();
		findAll = OrderDAO.findAllOrders();
		return findAll;
    }
}
