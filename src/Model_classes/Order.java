package Model_classes;


public class Order {

	private int O_ID;
	private int C_ID;
	private int P_ID;
	private int quantity;
	
	public Order(int O_ID, int C_ID, int P_ID, int quantity) {
	    this.O_ID = O_ID;
	    this.C_ID = C_ID;
	    this.P_ID = P_ID;
	    this.quantity = quantity;
	}
	
	public int getO_ID() {
		return O_ID;
	}
	
	public void setO_ID(int O_ID) {
		this.O_ID = O_ID;
	}
	
	public int getC_ID() {
		return C_ID;
	}
	
	public void setC_ID(int C_ID) {
		this.C_ID = C_ID;
	}
	
	public int getP_ID() {
		return P_ID;
	}
	
	public void setP_ID(int P_ID) {
		this.P_ID = P_ID;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@Override
	public String toString() {
		return "Order [ ID: " +O_ID+ ", C_ID: " +C_ID+ ", P_ID: " +P_ID+ ", quantity: " +quantity+ " ]";
	}	
}
