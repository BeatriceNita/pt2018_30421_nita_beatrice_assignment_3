package Model_classes;

public class Product {

	private int P_ID;
	private double price;
	private String P_name;
	private int stock;
	
	
	public Product (int P_ID, double price, String P_name, int stock) {
	    this.P_ID = P_ID;
	    this.price = price;
	    this.P_name = P_name;
	    this.stock = stock;
	}
	
	public int getP_ID() {
		return P_ID;
	}
	
	public void setP_ID(int P_ID) {
		this.P_ID = P_ID;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getP_name() {
		return P_name;
	}
	
	public void setP_name(String P_name) {
		this.P_name = P_name;
	}
	
	public int getStock() {
		return stock;
	}
	
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	@Override
	public String toString() {
		return "Product [ ID: " +P_ID+  ", price: " +price+ ", name: " +P_name+ ", stock: " +stock+ " ]";
	}	
}
