package Model_classes;

public class Client {
	
	private int C_ID;
	private int age;
	private String C_name;
	private String address;
	
	public Client (int C_ID, String C_name, int age, String address) {
		this.C_ID = C_ID;
		this.age = age;
		this.C_name = C_name;
		this.address = address;
	}
	
	public int getC_ID() {
		return C_ID;
	}
	
	public void setC_ID(int C_ID) {
		this.C_ID = C_ID;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getC_Name() {
		return C_name;
	}
	
	public void setC_Name(String C_name) {
		this.C_name = C_name;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		return C_ID+ "   " +C_name+ "   " + age+ "   " +address;
	}
}
