package Data_access_classes;

import java.sql.*;
import java.util.ArrayList;

import javax.swing.JTable;

import Model_classes.Client;
import Model_classes.Reflection;
import Presentation_classes.NrRows;
import connection.ConnectionFactory;

public class ClientDAO {

	private static String insertStatement = "INSERT INTO client (C_name, age, address)"+" VALUES (?,?,?)";
	private static String updateStatement = "UPDATE client SET C_name=?, age=?, address=? WHERE C_ID=?";
	private static String deleteStatement = "DELETE FROM client WHERE C_ID=?";
	private static String findStatement = "SELECT * FROM client WHERE C_ID=?";
	private static String statement = "SELECT * FROM client";
	
	public static int insertClient(Client client) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement insertS = null;
		
		int insertedID = -1;
		
		try 
		{
			insertS = dbConn.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);
			
			insertS.setString(1, client.getC_Name());
			insertS.setInt(2, client.getAge());
			insertS.setString(3, client.getAddress());
			
			insertS.executeUpdate();
			
			ResultSet rs = insertS.getGeneratedKeys();
			if (rs.next()) {
				insertedID = rs.getInt(1);
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return insertedID;
	}
	
	public static void updateClient(int C_ID, String C_Name, int age, String address) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement updateS = null;
		
		try 
		{
			updateS = dbConn.prepareStatement(updateStatement, Statement.RETURN_GENERATED_KEYS);
			
			updateS.setString(1, C_Name);
			updateS.setInt(2, age);
			updateS.setString(3, address);
			updateS.setInt(4, C_ID);
			
			updateS.executeUpdate();
			
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public static void deleteClient(int C_ID) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement deleteS = null;
		
		try 
		{
			deleteS = dbConn.prepareStatement(deleteStatement, Statement.RETURN_GENERATED_KEYS);
			deleteS.setInt(1, C_ID);
			
			deleteS.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public static Client findByID(int C_ID) {
		Client searchedClient = null;
		
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement findSID = null;
		ResultSet rs = null;
		
		try 
		{
			findSID = conn.prepareStatement(findStatement, Statement.RETURN_GENERATED_KEYS);
			findSID.setInt(1, C_ID);
			rs = findSID.executeQuery();
			rs.next();
			
			int age = rs.getInt("age");
			String name = rs.getString("C_name");
			String address = rs.getString("address");
			
			searchedClient = new Client(C_ID, name, age, address);
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return searchedClient;
	}
	
	public static ArrayList<Client> findAllClients(){

		ArrayList<Client> allClients = new ArrayList<Client>();
		
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement findAll = null;
		ResultSet rs = null;

		try {
			findAll = conn.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS);
			rs = findAll.executeQuery();

			while(rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);
				int age = rs.getInt(3);
				String address = rs.getString(4);
				
				Client newClient = new Client(id, name, age, address);
				
				allClients.add(newClient);			
			}
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return allClients;
	}
	
	public static JTable buildTable(ArrayList<Client> clientList){
		
		String columns[] = new String[4];//the headers
		String values[] = new String[5];//the data
	
		Reflection.retrieveFieldNames(clientList.get(0), columns);
		
		int nrRows = NrRows.countRowsClient(clientList.get(0));
		
		String data[][] = new String[nrRows][4];
		
		for(int j = 0; j < clientList.size(); j++) {
			Reflection.retrieveProperties(clientList.get(j), values);
			data[j][0] = values[0];
			data[j][1] = values[1];	
			data[j][2] = values[2];	
			data[j][3] = values[3];	
		}
		
		JTable jt = new JTable(data, columns);
		return jt;
     }
	
	
}
