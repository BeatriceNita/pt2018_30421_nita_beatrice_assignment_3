package Data_access_classes;

import java.sql.*;
import java.util.ArrayList;

import javax.swing.JTable;

import Model_classes.Order;
import Model_classes.Product;
import Model_classes.Reflection;
import Presentation_classes.NrRows;
import connection.ConnectionFactory;

public class OrderDAO {

	private static String insertStatement = "INSERT INTO ordermanagement.order (C_ID, P_ID, quantity)"+" VALUES (?,?,?)";
	private static String updateStatement = "UPDATE ordermanagement.order SET C_ID=?, P_ID=?, quantity=? WHERE O_ID=?";
	private static String deleteStatement = "DELETE FROM ordermanagement.order WHERE O_ID=?";
	private static String findStatement = "SELECT * FROM ordermanagement.order WHERE O_ID=?";
	private static String statement = "SELECT * FROM ordermanagement.order";
	
	public static int insertOrder(Order order) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement insertS = null;
		
		int insertedID = -1;
		
		try 
		{
			insertS = dbConn.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);
			insertS.setInt(1, order.getC_ID());
			insertS.setInt(2, order.getP_ID());
			insertS.setInt(3, order.getQuantity());
	
			insertS.executeUpdate();
			
			ResultSet rs = insertS.getGeneratedKeys();
			if (rs.next()) {
				insertedID = rs.getInt(1);
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		return insertedID;
	}
	
	public static void updateOrder(int O_ID, int C_ID, int P_ID, int quantity) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement updateS = null;
		
		try 
		{
			updateS = dbConn.prepareStatement(updateStatement);
			updateS.setInt(1, C_ID);
			updateS.setInt(2, P_ID);
			updateS.setInt(3, quantity);
			updateS.setInt(4, O_ID);
			
			updateS.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public static void deleteOrder(int O_ID) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement deleteS = null;
		
		try 
		{
			deleteS = dbConn.prepareStatement(deleteStatement);
			deleteS.setInt(1, O_ID);
			
			deleteS.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public static Order findByID(int O_ID) {
		Order searchedOrder = null;
		
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement findSID = null;
		ResultSet rs = null;
		
		try 
		{
			findSID = conn.prepareStatement(findStatement);
			findSID.setInt(1, O_ID);
			rs = findSID.executeQuery();
			rs.next();
			
			int C_ID = rs.getInt("C_ID");
			int P_ID = rs.getInt("P_ID");
			int quantity = rs.getInt("quantity");
			
			searchedOrder = new Order(O_ID, C_ID, P_ID, quantity);
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		return searchedOrder;
	}
	
	public static ArrayList<Order> findAllOrders(){

		ArrayList<Order> allOrders = new ArrayList<Order>();
		
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement findAll = null;
		ResultSet rs = null;

		try {
			findAll = conn.prepareStatement(statement);
			rs = findAll.executeQuery();

			while(rs.next()) {
				int O_ID = rs.getInt("O_ID");
				int C_ID = rs.getInt("C_ID");
				int P_ID = rs.getInt("P_ID");
				int quantity = rs.getInt("quantity");
				
				Order newOrder = new Order(O_ID, C_ID, P_ID, quantity);
				
				allOrders.add(newOrder);			
			}
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return allOrders;
	}
	
	public static JTable buildTable(ArrayList<Order> orderList){
		
		String columns[] = new String[4];//the headers
		String values[] = new String[5];//the data
	
		Reflection.retrieveFieldNames(orderList.get(0), columns);
		
		int nrRows = NrRows.countRowsOrder(orderList.get(0));
		
		String data[][] = new String[nrRows][4];
		
		for(int j = 0; j < orderList.size(); j++) {
			Reflection.retrieveProperties(orderList.get(j), values);
			data[j][0] = values[0];
			data[j][1] = values[1];	
			data[j][2] = values[2];	
			data[j][3] = values[3];	
		}
		
		JTable jt = new JTable(data, columns);
		return jt;
     }
}
