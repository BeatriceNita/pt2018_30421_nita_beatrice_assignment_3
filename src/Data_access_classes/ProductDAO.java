package Data_access_classes;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JTable;

import Model_classes.Client;
import Model_classes.Product;
import Model_classes.Reflection;
import Presentation_classes.NrRows;
import connection.ConnectionFactory;

public class ProductDAO {
	
	private static String insertStatement = "INSERT INTO product (price,P_name,stock)"+" VALUES (?,?,?)";
	private static String updateStatement = "UPDATE product SET price=? WHERE P_ID=?";
	private static String updateStatement1 = "UPDATE product SET stock=? WHERE P_ID=?";
	private static String deleteStatement = "DELETE FROM product WHERE P_ID=?";
	private static String findStatement = "SELECT * FROM product WHERE P_ID=?";
	private static String statement = "SELECT * FROM product";
	
	public static int insertProduct(Product product) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement insertS = null;
		
		int insertedID = -1;
		
		try 
		{
			insertS = dbConn.prepareStatement(insertStatement, Statement.RETURN_GENERATED_KEYS);
			insertS.setDouble(1, product.getPrice());
			insertS.setString(2, product.getP_name());
			insertS.setInt(3, product.getStock());
			
			insertS.executeUpdate();
			
			ResultSet rs = insertS.getGeneratedKeys();
			if (rs.next()) {
				insertedID = rs.getInt(1);
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return insertedID;
	}
	
	public static void updatePrice(int P_ID, double price) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement updateS = null;
		
		try 
		{
			updateS = dbConn.prepareStatement(updateStatement);
			updateS.setDouble(1, price);
			updateS.setInt(2, P_ID);
			
			updateS.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public static void updateStock(int P_ID, int stock) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement updateS = null;
		
		try 
		{
			updateS = dbConn.prepareStatement(updateStatement1);
			updateS.setInt(1, stock);
			updateS.setInt(2, P_ID);
			
			updateS.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public static void deleteProduct(int P_ID) {
		Connection dbConn = ConnectionFactory.getConnection();
		PreparedStatement deleteS = null;
		
		try 
		{
			deleteS = dbConn.prepareStatement(deleteStatement);
			deleteS.setInt(1, P_ID);
			
			deleteS.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public static Product findByID(int P_ID) {
		Product searchedProduct = null;
		
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement findSName = null;
		ResultSet rs = null;
		
		try 
		{
			findSName = conn.prepareStatement(findStatement, Statement.RETURN_GENERATED_KEYS);
			findSName.setInt(1, P_ID);
			
			rs = findSName.executeQuery();
			rs.next();
			
			String P_name = rs.getString("P_name");
			double price = rs.getDouble("price");
			int stock = rs.getInt("stock");
			
			searchedProduct = new Product(P_ID, price, P_name, stock);
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return searchedProduct;
	}
	
	public static ArrayList<Product> findAllProducts(){

		ArrayList<Product> allProducts = new ArrayList<Product>();
		
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement findAll = null;
		ResultSet rs = null;

		try {
			findAll = conn.prepareStatement(statement);
			rs = findAll.executeQuery();

			while(rs.next()) {
				int P_ID = rs.getInt("P_ID");
				String P_name = rs.getString("P_name");
				double price = rs.getDouble("price");
				int stock = rs.getInt("stock");
				
				Product newProduct = new Product(P_ID, price, P_name, stock);
				
				allProducts.add(newProduct);			
			}
		}
		catch(SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
		}
		
		return allProducts;
	}
	
	public static JTable buildTable(ArrayList<Product> productList){
		
		String columns[] = new String[4];//the headers
		String values[] = new String[5];//the data
	
		Reflection.retrieveFieldNames(productList.get(0), columns);
		
		int nrRows = NrRows.countRowsProduct(productList.get(0));
		
		String data[][] = new String[nrRows][4];
		
		for(int j = 0; j < productList.size(); j++) {
			Reflection.retrieveProperties(productList.get(j), values);
			data[j][0] = values[0];
			data[j][1] = values[1];	
			data[j][2] = values[2];	
			data[j][3] = values[3];	
		}
		
		JTable jt = new JTable(data, columns);
		return jt;
     }
	
	
	/*public Boolean enoughProductsInStock(int P_ID, int quantity) {
		Boolean enoughStock = false;
		
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement enoughS = null;
		ResultSet rs = null;
		
		try 
		{
			enoughS = conn.prepareStatement(findStatement);
			enoughS.setInt(1, P_ID);
			rs = enoughS.executeQuery();

			System.out.println("Found"+ rs);
			
			while(rs.next()) 
			{
				
				int stock = rs.getInt("stock");
				if (stock >= quantity)	
				{
					enoughStock = true;
				}
				else enoughStock = false;
			}
		}
		catch(SQLException e) 
		{
			System.out.println("SQLException: " + e.getMessage());
		}
		return enoughStock;
    }
	
	public void reduceStock(int P_ID, int quantity) {
		int stock= 0;
		
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement reduceStock = null;
		ResultSet rs = null;
		
		try 
		{
			reduceStock = conn.prepareStatement(findStatement);
			reduceStock.setInt(1, P_ID);
			rs = reduceStock.executeQuery();

			while(rs.next()) 
			{
				stock = rs.getInt("stock");
			}
		}
		catch(SQLException e) 
		{
			System.out.println("SQLException: " + e.getMessage());
		}

		int reduced_stock = stock - quantity;
		
		try 
		{
			reduceStock = conn.prepareStatement(updateStatement);
			reduceStock.setInt(1, reduced_stock );
			reduceStock.setInt(2, P_ID );
			reduceStock.executeUpdate();

		}
		catch(SQLException e) 
		{
			System.out.println("SQLException: " + e.getMessage());
		}
	}
	
	public void increaseStock(int P_ID, int quantity) {
		int stock= 0;
		
		Connection conn = ConnectionFactory.getConnection();
		PreparedStatement increaseStock = null;
		ResultSet rs = null;
		try 
		{
			increaseStock = conn.prepareStatement(findStatement);
			increaseStock.setInt(1, P_ID);
			rs = increaseStock.executeQuery();

			while(rs.next()) 
			{
				stock = rs.getInt("stock");
			}
		}
		catch(SQLException e)
		{
			System.out.println("SQLException: " + e.getMessage());
		}

		int increased_stock = stock + quantity;
		try 
		{

		    increaseStock = conn.prepareStatement(updateStatement);
		    increaseStock.setInt(1, increased_stock );
		    increaseStock.setInt(2, P_ID );
		    increaseStock.executeUpdate();
		}
		catch(SQLException e) 
		{
			System.out.println("SQLException: " + e.getMessage());
		}
}*/

}
